package Exception;

import java.io.File;

public class VilleException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String s;
	private String i;
	private File fichier;
	
	public VilleException(String s, String i, File fichier) {
		super();
		this.s = s;
		this.i = i;
		this.fichier = fichier;
	}


	public String getS() {
		return s;
	}
	public String getNum() {
		return i;
	}
	public File getFichier() {
		return fichier;
	}
	
}
