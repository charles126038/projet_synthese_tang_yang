package Exception;

import java.io.File;

public class Erreur {
	
	public String ErreurMessage(VilleException source) {  // (des exception messages)
		String ville = source.getS();
		String tas = source.getNum();
		File fichier = source.getFichier();
		if(fichier == null)
			return "Please choose the file first";
		else {
			if(ville.trim().equals("ErreurVilleDeparte"))
				return "Can't find the city, please choose a new one";
			
			if(ville.trim().equals(""))
				return "You should enter a city's name";
			
			if(tas.trim().equals(""))
				return "You should enter a heap's number(0 tableaux dynamiques, 1 arbres binaires complets, 2 listes ordonees)";
			
			if(!tas.trim().equals("0") && !tas.trim().equals("1") && !tas.trim().equals("2"))
				return "You should enter a correct number of heap (0, 1, 2)";
		}
		
		return "";
	}
}
