package Traitement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import Exception.VilleException;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

public class traiter {
	private String villeDeparte;
	private String NumTas;
	private File fichier;
	private Group p;
	private File solutionFile;
	DrawMap map;
	
	/** en C Dijkstra
	 * 
	 * @param inFileName
	 * @param outFileName
	 * @param sourceName
	 * @param heaptype
	 */
	public native void Dijkstra(String inFileName, String outFileName, String sourceName, int heaptype);
	
	static {
		System.loadLibrary("dijkstra");
	}
	
	public traiter(String villeDeparte, String numTas, File fichier, Group p) {
		super();
		this.villeDeparte = villeDeparte;
		NumTas = numTas;
		this.fichier = fichier;
		this.p = p;
	}
	

	public File getSolutionFile() {
		return solutionFile;
	}


	public DrawMap getMap() {
		return map;
	}


	/**
	 * Traiter le graphe
	 * @return le graphe
	 * @throws VilleException
	 */
	public Group treat() throws VilleException{
		// des exceptions
		if(fichier == null)  // si il trouve pas le fichier
			throw new VilleException(villeDeparte, NumTas, fichier);
		else if(!getNomFromFile(villeDeparte))  // si il trouve pas la ville
			throw new VilleException("ErreurVilleDeparte", NumTas, fichier);
		else if(villeDeparte.trim().equals(""))  // si aucune nom de la ville saisie
			throw new VilleException(villeDeparte, NumTas, fichier);
		else if(NumTas.trim().equals(""))  // si aucun numero du tas saisie
			throw new VilleException(villeDeparte, NumTas, fichier);
		else if(!NumTas.trim().equals("0") && !NumTas.trim().equals("1") && !NumTas.trim().equals("2"))  // si le numero du tas est incorrect
			throw new VilleException(villeDeparte, NumTas, fichier);
		else {// les methodes
			/**
			 * connexion avec langage C
			 */
			int heaptype = Integer.parseInt(NumTas);
			traiter dijk = new traiter(villeDeparte, NumTas, fichier, p);
			dijk.Dijkstra(fichier.toString(), "solution_map.txt", villeDeparte, heaptype);
			solutionFile = new File("solution_map.txt");
			
			// afficher la fenetre avec le graphe

			map = new DrawMap(fichier, villeDeparte, solutionFile);
			 
			FXMLLoader loader = new FXMLLoader(getClass().getResource("MapView.fxml"));

			loader.setController(map);
			Group grille = null;
			try {
				grille = loader.load();
			} catch (IOException e) {
				e.printStackTrace();
			}
			map.MapCreation();
			
			return p = grille;
		}
	}
	
	/** Creer les bouttons des villes a partir de ville departe par le plus court chemin
	 * 
	 * @return une liste de bouttons des villes
	 */
	public ScrollPane lesButtons() {
		ScrollPane scrollPane = new ScrollPane();
		FlowPane content = new FlowPane();
		try {
			
			BufferedReader br = new BufferedReader(new FileReader(fichier));
			String line;
			int n = 0;
			while(true) {
				line = br.readLine();
				if(line != null) {
					n = Integer.parseInt(line);
					break;
				}
				else
					break;
			}
			
			
			for(int i = 0; i < n; i++){
				line = br.readLine();
				if(!line.trim().equals(villeDeparte)) {
					Controler buttonChemin = new Controler(fichier, villeDeparte, solutionFile, line, p, map.getChoix());
					Button ville = new Button(line);
					ville.addEventHandler(ActionEvent.ACTION, buttonChemin);
					ville.setMinWidth(100);
					ville.setMaxWidth(100);
					DropShadow shadow = new DropShadow();
					shadow.setColor(Color.valueOf("#19588c"));
					ville.setEffect(shadow);
					content.getChildren().add(ville);
					
					
					ville.setCursor(Cursor.HAND);
					ville.setId("button");
					
				}
			}
			
			br.close();
		}catch (Exception ex){
			ex.printStackTrace(); 
		}
		content.setPadding(new Insets(10,40,10,40));
		content.setHgap(7);
		content.setVgap(12);
		content.setMaxWidth(350);
		content.setId("scroll-pane");
		scrollPane.setPadding(new Insets(0, 0, 20, 0));
		scrollPane.setId("scroll-pane");;
		scrollPane.setContent(content);
		scrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
		scrollPane.setVbarPolicy(ScrollBarPolicy.ALWAYS);

		return scrollPane;
	}
	
	/** parcourir le fichier pour trouver la ville departe
	 * 
	 * @param nom de la ville
	 * @return si le fichier contient la ville
	 */
	private boolean getNomFromFile(String s) { 
		try { 
			BufferedReader br = new BufferedReader(new FileReader(fichier)); 
			String line;
			int n = 0;
			while(true) {
				line = br.readLine();
				if(line != null) {
					n = Integer.parseInt(line);
					break;
				}
				else
					break;
			}

			for(int i = 0; i < n; i++){
				line = br.readLine();
				if(line.contentEquals(s)) {
					br.close();
					return true;
				}
			}
			br.close();
		}catch (Exception ex){
			ex.printStackTrace(); 
		}
		return false;
	}
	
	/**
	 * Choisir un style du graphe
	 * @return 
	 */
	public ComboBox<String> StyleGraphe(GridPane buttonPane){
		ComboBox<String> myComboBox = new ComboBox<String>();
		myComboBox.getItems().addAll("Aleatoire", "Cercle");
	    myComboBox.setValue("Cercle");
	   
	    myComboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue)->{
            	FXMLLoader loader = new FXMLLoader(getClass().getResource("MapView.fxml"));
         	    loader.setController(map);
         	    Group paneGroup = null;
         	    try {
         	    	paneGroup = loader.load();
         	    } catch (IOException e) {
         	    	e.printStackTrace();
         	    }
         	    map.setChoix(newValue);
	        	map.MapCreation();
	        	p.getChildren().clear();
	        	p.getChildren().add(paneGroup);
	        	
	        	//reconstruire les boutton pour change les actions sur Alea ou Cercle
	        	buttonPane.getChildren().add(lesButtons());
            }
        );
	    return myComboBox;
	}

	@Override
	public String toString() {
		return "traiter [villeDeparte=" + villeDeparte + ", NumTas=" + NumTas + ", fichier=" + fichier + "]";
	}
}
