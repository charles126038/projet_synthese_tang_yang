package Traitement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class DrawMap {
	@FXML Group _pane;
	private File fichierIn;
	private File fichierOut;
	private String villeDeparte;
	private String destination;
	private String choix;
	
	public DrawMap(File fichier, String villeDeparte, File fichierout) {
		super();
		this.fichierIn = fichier;
		this.villeDeparte = villeDeparte;
		this.fichierOut = fichierout;
		this.destination = "";
		this.choix = "Cercle";
	}
	
	

	public String getDestination() {
		return destination;
	}



	public void setDestination(String destination) {
		this.destination = destination;
	}



	public String getChoix() {
		return choix;
	}



	public void setChoix(String choix) {
		this.choix = choix;
	}



	public Group getPane() {
		return _pane;
	}
	
	//  Afficher le graphe
	public void MapCreation() {
		try { 
			ArrayList<Point2D> ListduCoor;
			ArrayList<String> ListduNom = ListNom();
			
			if(choix.trim().equals("Aleatoire"))
				 ListduCoor = ListPointAlea(ListduNom);
			else
				ListduCoor = ListPointCercle(ListduNom);
			
			ArrayList<String> plusCourtChemin = null;
			ArrayList<Line> areteCourtChemin = null;
			if(!destination.trim().equals("")) {
				plusCourtChemin = cheminVille(destination);
				areteCourtChemin = cheminArete(ListduNom, ListduCoor, plusCourtChemin);
			}
			
			ArrayList<String> ListKM = ListKM();
			ArrayList<Line> ListAretes = ListLine(ListduNom, ListduCoor);
		
			
			//--------------designer les aretes--------------------
			int i = 0;
			for(Line arete:ListAretes) {
				Text km = new Text(ListKM.get(i));
				km.setFill(Color.WHITE);
				km.setX((arete.getEndX() - arete.getStartX())/5 + arete.getStartX());
				km.setY((arete.getEndY() - arete.getStartY())/5 + arete.getStartY());
				_pane.getChildren().addAll(arete, km);
				i++;
			}
			
			
			//-------------designer les villes----------------------
			int j = 0;
			for(String ville:ListduNom) {
				Text nom = new Text(ville);
				nom.setFill(Color.WHITE);
				Circle v = new Circle(ListduCoor.get(j).getX(), ListduCoor.get(j).getY(), 5);
				nom.setX(ListduCoor.get(j).getX() + 10);
				nom.setY(ListduCoor.get(j).getY() + 10);
				v.setStroke(Color.WHITE);
				if(ville.trim().equals(villeDeparte)) {
					v.setFill(Color.HOTPINK);
					v.setRadius(10);
				}
				else
					v.setFill(Color.AQUAMARINE);
				DropShadow shadow = new DropShadow();
				v.setEffect(shadow);
				_pane.getChildren().addAll(v, nom);
				j++;
			}
			
			//designer les plus courts chemins
			if(plusCourtChemin != null) {
				//--------------designer les aretes--------------------
				for(Line arete:areteCourtChemin) {
					_pane.getChildren().addAll(arete);
				}
				
				//--------------designer les villes--------------------
				for(String ville:plusCourtChemin) {
					int pos = ListduNom.indexOf(ville);
					Circle v = new Circle(ListduCoor.get(pos).getX(), ListduCoor.get(pos).getY(), 5);
					DropShadow shadow = new DropShadow();
					v.setEffect(shadow);
					v.setStroke(Color.CORNSILK);
					v.setFill(Color.CORAL);
					v.setStrokeWidth(3);
					if(ville.trim().equals(villeDeparte)) {
						v.setFill(Color.HOTPINK);
						v.setRadius(10);
					}
					_pane.getChildren().addAll(v);
				}
			}
			
				
		}catch (Exception ex){
			ex.printStackTrace(); 
		}
		
	}
	
	/**
	 * Designer les chemins
	 * @param villes  liste des tous villes
	 * @param Coordonnes  liste de coordonnes
	 * @param chemin  liste de plus court chemin
	 * @return
	 */
	public ArrayList<Line> cheminArete(ArrayList<String> villes, ArrayList<Point2D> Coordonnes, ArrayList<String> chemin){
		ArrayList<Line> lines= new ArrayList<Line>();
		for(int i = 0; i < chemin.size() - 1; i++) {
			int indice1 = villes.indexOf(chemin.get(i));
			int indice2 = villes.indexOf(chemin.get(i + 1));
			Line arete = new Line(Coordonnes.get(indice1).getX(), Coordonnes.get(indice1).getY(), Coordonnes.get(indice2).getX(), Coordonnes.get(indice2).getY());
			arete.setStroke(Color.DARKORANGE);
			arete.setStrokeWidth(3);
			lines.add(arete);
		}
		return lines;
	}

	
	/**
	 * retourner une liste de chemin plus court
	 * @param destination  la ville destinee
	 * @return
	 * @throws IOException
	 */
	public ArrayList<String> cheminVille(String destination) throws IOException{
		ArrayList<String> dic = CheminNomVilles();
		ArrayList<String> villes = new ArrayList<>();
		villes.add(destination);
		while(!destination.trim().equals(villeDeparte)) {
			int v1 = dic.indexOf(destination);
			while(v1%2 != 0) {
				dic.set(v1, "null");
				v1 = dic.indexOf(destination);
			}
			villes.add(dic.get(v1 + 1));
			destination = dic.get(v1 + 1);
		}
		return villes;
	}
	
	/**
	 * Creer une dictionaire des villes
	 * @return
	 * @throws IOException
	 */
	public ArrayList<String> CheminNomVilles() throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(fichierOut)); 
		String line;
		String[] content;
		int n = 0;
		while(true) {  //obtenir le nombre de villes
			line = br.readLine();
			if(line != null) {
				n = Integer.parseInt(line);
				break;
			}
			else
				break;
		}
		
		while(true) {  //skip la ville departe
			line = br.readLine();
			break;
		}
		
		ArrayList<String> list = new ArrayList<String>();
		for(int i = 0; i < n - 1; i++) {
			line = br.readLine();
			content = line.split(" ");
				list.add(content[0]);
				list.add(content[2]);
		}
		br.close();
		
		return list;
	}
	
	
	/**creer une liste de nom des villes
	 * 
	 * @return list des noms de villes
	 * @throws IOException
	 */
	public ArrayList<String> ListNom() throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(fichierOut)); 
		String line;
		String[] content;
		int n = 0;
		while(true) {  //obtenir le nombre de villes
			line = br.readLine();
			if(line != null) {
				n = Integer.parseInt(line);
				break;
			}
			else
				break;
		}
		
		while(true) {  //skip la ville departe
			line = br.readLine();
			break;
		}
		
		ArrayList<String> list = new ArrayList<String>();
		for(int i = 0; i < n - 1; i++) {
			line = br.readLine();
			content = line.split(" ");
			if(!list.contains(content[0]))
				list.add(content[0]);
			if(!list.contains(content[2]))
				list.add(content[2]);
		}
		br.close();
		
		return list;
	}
	
	/**creer une liste des coordonnees (Aleatoirement)
	 * 
	 * @param liste de villes
	 * @return liste de coordonnes des villes
	 */
	public ArrayList<Point2D> ListPointAlea(ArrayList<String> villes) {	
		ArrayList<Point2D> list = new ArrayList<Point2D>();
		int[] listX, listY;
		if(villes.size() < 15) {
			listX = randomNumber(500, 1000, villes.size());
			listY = randomNumber(500, 1000, villes.size());
		}
		else if(villes.size() < 30){
			listX = randomNumber(750, 1500, villes.size());
			listY = randomNumber(750, 1500, villes.size());
		}
		else {
			listX = randomNumber(2000, 4000, villes.size());
			listY = randomNumber(2000, 4000, villes.size());
		}
		
		for(int i = 0; i < villes.size(); i++) {
			Point2D c = new Point2D(listX[i], listY[i]);
			list.add(c);		
		}
		return list;
	}
	
	/**creer une liste des coordonnees (Cercle)
	 * 
	 * @param liste de villes
	 * @return liste de coordonnes des villes
	 */
	public ArrayList<Point2D> ListPointCercle(ArrayList<String> villes){
		ArrayList<Point2D> list = new ArrayList<Point2D>();
		Point2D centre = new Point2D(750, 750);
		double angle = 0.0;
		for(int i = 0; i < villes.size(); i++) {
			angle = angle + 360.0/(double)villes.size();
			Point2D c;
			if(villes.size() < 15) {
				c = cerclePoint(centre, angle, 250);
			}
			else if(villes.size() < 30){
				c = cerclePoint(centre, angle, 350);
			}
			else {
				c = cerclePoint(centre, angle, 700);
			}
			
			list.add(c);
		}
		
		
		return list;
	}
	
	/**creer une liste de distances
	 * 
	 * @return liste de distances
	 * @throws IOException
	 */
	public ArrayList<String> ListKM() throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(fichierIn)); 
		String line;
		String[] content;
		int n = 0;
		while(true) {  //obtenir le nombre de villes
			line = br.readLine();
			if(line != null) {
				n = Integer.parseInt(line);
				break;
			}
			else
				break;
		}
		
		for(int i = 0; i < n; i++) {  //skip les villes
			line = br.readLine();
		}
		
		while(true) {  //obtenir le nombre des aretes
			line = br.readLine();
			if(line != null) {
				n = Integer.parseInt(line);
				break;
			}
			else
				break;
		}
		
		ArrayList<String> list = new ArrayList<String>();
		for(int i = 0; i < n; i++) {
			line = br.readLine();
			content = line.split(" ");
			list.add(content[5]);
		}
		br.close();
		
		return list;
	}

	/**creer une liste des arestes
	 * 
	 * @param liste des villes
	 * @param liste des Coordonnes
	 * @return liste des aretes
	 * @throws IOException
	 */
	public ArrayList<Line> ListLine(ArrayList<String> ville, ArrayList<Point2D> Coord) throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(fichierIn)); 
		String line;
		String[] content;
		int n = 0;
		while(true) {  //obtenir le nombre de villes
			line = br.readLine();
			if(line != null) {
				n = Integer.parseInt(line);
				break;
			}
			else
				break;
		}
		
		for(int i = 0; i < n; i++) {  //skip les villes
			line = br.readLine();
		}
		
		while(true) {  //obtenir le nombre des aretes
			line = br.readLine();
			if(line != null) {
				n = Integer.parseInt(line);
				break;
			}
			else
				break;
		}
		
		ArrayList<Line> lines= new ArrayList<Line>();
		for(int i = 0; i < n; i++) {
			line = br.readLine();
			content = line.split(" ");
			int indice1 = ville.indexOf(content[1]);
			int indice2 = ville.indexOf(content[3]);
			Line arete = new Line(Coord.get(indice1).getX(), Coord.get(indice1).getY(), Coord.get(indice2).getX(), Coord.get(indice2).getY());
			arete.setFill(Color.CORNSILK);
			arete.setStroke(Color.CORNSILK);	
			lines.add(arete);
		}
		br.close();
		return lines;
	}

	
	/**creer les values aleatoires
	 * 
	 * @param min
	 * @param max
	 * @param nombre des valeurs
	 * @return une valeur aleatoire
	 */
	private static int[] randomNumber(int min,int max,int n){
        if(n>(max-min+1) || max <min){
            return null;
        }

        int[] result = new int[n];

        int count = 0;
        while(count <n){
            int num = (int)(Math.random()*(max-min))+min;
            boolean flag = true;
            for(int j = 0; j < count; j++){
                if(num == result[j]){
                    flag = false;
                    break;
                }
            }
            if(flag){
                result[count] = num;
                count++;
            }
        }
        return result;
    } 
	
	/**
	 * Un point sur un cercle
	 * @param centre   le centre du cercle
	 * @param angle		l'angle
	 * @param r			le rayon
	 * @return
	 */
	private static Point2D cerclePoint(Point2D centre, double angle, int r) {
		double a1 = Math.toRadians(angle);
		
		double x = (centre.getX() + r *  Math.cos(a1));
		double y = (centre.getY() + r *  Math.sin(a1));
		
		return new Point2D(x, y);
	}
}

