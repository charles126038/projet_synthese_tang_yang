package Traitement;

import java.io.File;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;

public class Controler implements EventHandler<ActionEvent>{
	private File fichier;
	private String villeDeparte;
	private File fichierout;
	private String destination;
	private Group p;
	private String choix;
	
	
	
	public Controler(File fichier, String villeDeparte, File fichierout, String destination, Group g, String choix) {
		super();
		this.fichier = fichier;
		this.villeDeparte = villeDeparte;
		this.fichierout = fichierout;
		this.destination = destination;
		this.p = g;
		this.choix = choix;
	}


	@Override
	public void handle(ActionEvent event) {
		DrawMap selectChemin = new DrawMap(fichier, villeDeparte, fichierout);
		selectChemin.setChoix(choix);
		FXMLLoader loader = new FXMLLoader(getClass().getResource("MapView.fxml"));
 	    loader.setController(selectChemin);
 	    Group paneGroup = null;
 	    try {
 	    	paneGroup = loader.load();
 	    } catch (IOException e) {
 	    	e.printStackTrace();
 	    }
    	selectChemin.setDestination(destination);
    	selectChemin.MapCreation();
    	p.getChildren().clear();
    	p.getChildren().add(paneGroup);
	}
	
}
