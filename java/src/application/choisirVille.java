package application;

import Exception.VilleException;

import java.io.File;

import Exception.Erreur;
import Traitement.traiter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


public class choisirVille {
	@FXML private TextField NumTas;
	@FXML private TextField villeDeparte;
	@FXML private Group _fxGroup;
	@FXML private GridPane _fxGridPane;
	@FXML private Label _toVille;
	@FXML private FlowPane _choix;
	private File file;
	

	@FXML
	public void draw(ActionEvent event){
		traiter t = new traiter(villeDeparte.getText(), NumTas.getText(), file, _fxGroup);
		try {
			// designer pane
			if(_fxGroup.getChildren().isEmpty())
				_fxGroup.getChildren().add(t.treat());
			else {
				Group iter = t.treat();
				_fxGroup.getChildren().clear();
				_fxGroup.getChildren().add(iter);
			}
			
			// button pane
			_toVille.setText("Dans quelle ville voulez-vous aller:");
			_toVille.setTextFill(Color.BEIGE);
			if(_fxGridPane.getChildren().isEmpty())
				_fxGridPane.getChildren().add(t.lesButtons());
			else {
				ScrollPane f = t.lesButtons();
				_fxGridPane.getChildren().clear();
				_fxGridPane.getChildren().add(f);
			}
			
			//choix du style
			if(_choix.getChildren().isEmpty()) {
				if(_fxGridPane.getChildren().isEmpty())
					_choix.getChildren().add(t.StyleGraphe(_fxGridPane));
				else {
					ScrollPane f = t.lesButtons();
					_fxGridPane.getChildren().clear();
					_fxGridPane.getChildren().add(f);
					_choix.getChildren().add(t.StyleGraphe(_fxGridPane));
				}
			}
			else {	
				_choix.getChildren().clear();
				if(_fxGridPane.getChildren().isEmpty())
					_choix.getChildren().add(t.StyleGraphe(_fxGridPane));
				else {
					ScrollPane f = t.lesButtons();
					_fxGridPane.getChildren().clear();
					_fxGridPane.getChildren().add(f);
					_choix.getChildren().add(t.StyleGraphe(_fxGridPane));
				}
			}
			
		} catch (VilleException e) {  // si on a saisi un nom incorrect
			Erreur erreur = new Erreur();
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error!");
			alert.setContentText(erreur.ErreurMessage(e));
			alert.showAndWait();
		}
	}
	
	
	 @FXML
	private void load(ActionEvent event) {  // import un fichier
		 FileChooser fileChooser = new FileChooser();
		 fileChooser.setTitle("Select the file to import");
		 file = fileChooser.showOpenDialog(new Stage());
	}
	
	@FXML
    private void handleQuitButtonAction(ActionEvent event) {
        System.exit(0);
    }
}
