#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "dyntable.h"
#include "tree.h"
#include "list.h"
#include "heap.h"
#include "town.h"

HNode * newHNode(int value, void *data) {
	HNode * node = malloc(sizeof(HNode));
	node->data = data;
	node->value = value;
	node->ptr = NULL;
	return node;
}

// type =
//    0 (DynTableHeap)
//    1 (CompleteBinaryTreeHeap)
//    2 (ListHeap)
Heap * newHeap(int type) {
	assert(type == 0 || type == 1 || type == 2);
	Heap* H = calloc(1, sizeof(Heap));
	H->dict = newDTable();
	switch (type) {
		case 0:
			H->heap = newDTable();
			H->HeapInsert = DTHeapInsert;
			H->HeapExtractMin = DTHeapExtractMin;
			H->HeapIncreasePriority = DTHeapIncreasePriority;
			H->viewHeap = viewDTHeap;
			break;
		case 1:
			H->heap = newCBTree();
			H->HeapInsert = CBTHeapInsert;
			H->HeapExtractMin = CBTHeapExtractMin;
			H->HeapIncreasePriority = CBTHeapIncreasePriority;
			H->viewHeap = viewCBTHeap;
			break;
		case 2:
			H->heap = newList();
			H->HeapInsert = OLHeapInsert;
			H->HeapExtractMin = OLHeapExtractMin;
			H->HeapIncreasePriority = OLHeapIncreasePriority;
			H->viewHeap = viewOLHeap;
			break;
	}
	return H;
}

/**********************************************************************************
 * DYNAMIC TABLE HEAP
 **********************************************************************************/

void* DTHeapInsert(Heap *H, int value, void *data) {
	HNode* v = newHNode(value, data);
	DTable* t = H->heap;
	DTableInsert(t, v);  //inserer dans la derniere place
	int indice = t->used - 1;
	int* i = (int*)malloc(sizeof(int));  // i : data de dict
	*i = t->used - 1;
	int* postion = (int*)malloc(sizeof(int));  // postion : indice de dict
	*postion = t->used - 1;
	if (indice == 0) //si c'est le premier noeud
		H->dict = newDTable();
	DTableInsert(H->dict, i);  // mettre a jour de la dict
	v->ptr = postion;

	HNode* pere = t->T[(indice - 1) / 2];
	while (indice != 0 && v->value < pere->value) {  //echanger les positions
		HNode* n1 = t->T[indice];           // trouver les indices dans dict correspendent a tableD
		HNode* n2 = t->T[(indice - 1) / 2];
		int pos1 = *(int*)n1->ptr;
		int pos2 = *(int*)n2->ptr;

		DTableSwap(t, indice, (indice - 1) / 2);
		DTableSwap(H->dict, pos1, pos2);

		indice = (indice - 1) / 2;
		pere = t->T[(indice - 1) / 2];
	}
	return postion;
}

HNode * DTHeapExtractMin(Heap* H) {
	DTable* T = H->heap;
	HNode* v;
	if (T->used == 0)
		return NULL;
	else if (T->used == 1) {  //si il existe seulement 1 noeud
		v = DTableRemove(T);
	}
	else if (T->used == 2) {  //si il existe 2 noeud sans fils droite
		DTableSwap(T, 0, 1);
		DTableSwap(H->dict, 0, 1);
		v = DTableRemove(T);
	}
	else {  // si il existe plusieurs noeuds
		HNode* np = T->T[0];           // trouver les indices dans dict correspendent a tableD
		HNode* nd = T->T[T->used - 1];
		int posP = *(int*)np->ptr;
		int posD = *(int*)nd->ptr;

		DTableSwap(T, 0, T->used - 1);  // echanger le premier noeud avec le dernier
		DTableSwap(H->dict, posP, posD);
		v = DTableRemove(T);

		int posCourant = 0;  //on enregistre la position courant
		HNode* iterator = T->T[posCourant];
		HNode* fg = T->T[posCourant * 2 + 1];
		HNode* fd;
		if ((posCourant * 2 + 2) < T->used)
			fd = T->T[posCourant * 2 + 2];
		else {
			if (iterator->value > fg->value) {
				np = T->T[0];           // trouver les indices dans dict correspendent a tableD
				nd = T->T[T->used - 1];
				int posP = *(int*)np->ptr;
				posD = *(int*)nd->ptr;
				DTableSwap(T, 0, T->used - 1);
				DTableSwap(H->dict, posP, posD);	
			}
			return v;
		}
		HNode* min;
		int posMin;
		if (fg->value < fd->value) {  //on compare les priorites des deux fils
			min = fg;
			posMin = posCourant * 2 + 1;
		}
		else {
			min = fd;
			posMin = posCourant * 2 + 2;
		}
		while (iterator->value > min->value) {  //echanger le noeud dans la bonne position
			HNode* n1 = T->T[posCourant];           // trouver les indices dans dict correspendent a tableD
			HNode* n2 = T->T[posMin];
			int pos1 = *(int*)n1->ptr;
			int pos2 = *(int*)n2->ptr;

			DTableSwap(T, posCourant, posMin);  //echanger le noeud avec son fils qui a la value plus petite
			DTableSwap(H->dict, pos1, pos2);
			posCourant = posMin;
			iterator = T->T[posCourant];
			
			if ((posCourant * 2 + 1) < T->used){   //si il a un fils gauche
				fg = T->T[posCourant * 2 + 1];
				min = fg;
				posMin = posCourant * 2 + 1;
				if ((posCourant * 2 + 2) < T->used) {  //si il a un fils gauche et un fils droite
					fd = T->T[posCourant * 2 + 2];
					if (fg->value > fd->value) {
						min = fd;
						posMin = posCourant * 2 + 2;
					}
				}
			}
			else {
				break;
			}
		}
	}
	return v;
}

void DTHeapIncreasePriority(Heap* H, void *position, int value) {
	int* ind = (int*) position;
	DTable* T = H->heap;
	HNode* iterator = T->T[*ind];
	iterator->value = value;              //increase the priority
	int posP = (*ind - 1) / 2;
	int pos = *ind;
	HNode* pere = T->T[posP];
	while (pere && iterator && pere->value > iterator->value) {  //faire monter
 		HNode* n1 = T->T[pos];           // trouver les indices dans dict correspendent a tableD
		HNode* n2 = T->T[posP];
		int pos1 = *(int*)n1->ptr;
		int pos2 = *(int*)n2->ptr;

		DTableSwap(T, pos, posP);
		DTableSwap(H->dict, pos1, pos2);
		if (posP > 0) {
			posP = (posP - 1) / 2;
			pere = T->T[posP];
			pos = (pos - 1) / 2;
			iterator = T->T[pos];
		}
		else
			break;
	}
}

void viewDTHeap(const Heap* H, void (*ptrf)(const void*)) {
	viewDTable(H->heap, ptrf);
}

/**********************************************************************************
 * COMPLETE BINARY TREE HEAP
 **********************************************************************************/

void* CBTHeapInsert(Heap *H, int value, void *data) {
	CBTree* tree = H->heap;
	HNode* node = newHNode(value, data);

	CBTreeInsert(tree, node); //inserer au dernier niveau le plus a gauche possible
	TNode* v = tree->last;  //on enregistre le dernier noeud
	if (tree->numelm != 1) {  //si l'arbre a plusieur noeuds
		HNode* iterator;
		HNode* parent;
		iterator = v->data;
		parent = v->parent->data;

		if (iterator && parent) {  //si le noeud et son parent ne sont pas NULL
			while (iterator->value < parent->value) { // si la value de ce noeud est plus petite que cella de son parent
				CBTreeSwap(tree, v->parent, v);  //echanger le noeud avec son parent
				if (v->parent)
					parent = v->parent->data;
				else
					break;  // on s'arrete quand le noeud arrive a la racine
			}
		}
	}
	return v;
}

HNode * CBTHeapExtractMin(Heap *H) {
	CBTree* tree = H->heap;
	HNode* v;
	if (tree->numelm == 0)
		return NULL;
	if(tree->numelm == 1)  //si l'arbre ne contient qu'un seule noeud
		v = CBTreeRemove(tree);
	else {
		CBTreeSwapRootLast(tree);  //echanger last et root
		v = CBTreeRemove(tree);  //supprimer last
		TNode* racine = tree->root;
		TNode* fg = racine->left;
		TNode* fd = racine->right;
		if (fg && fd) {
			HNode* e0 = racine->data;
			HNode* e1 = fg->data;
			HNode* e2 = fd->data;
			TNode* min;
			while (e0->value > e1->value || e0->value > e2->value) {  //si le priorite est plus petit que ses fils
				if (e1->value < e2->value)
					min = fg;
				else
					min = fd;
				CBTreeSwap(tree, racine, min); // descendre la racine a la bonne position en echangeant avec son plus petit fils
				fg = racine->left;
				fd = racine->right;
				if (fg)
					e1 = fg->data;
				else
					break;

				if (fd)
					e2 = fd->data;
				else
					break;
			}
		}
	}
	return v;
}

void CBTHeapIncreasePriority(Heap *H, void *tnode, int value) {
	CBTree* tree = H->heap;
	TNode* node = tnode;
	HNode* v = node->data;
	HNode* vp;
	if (node->parent) {
		vp = node->parent->data;
		if (v->value < value) {  //il faut augmenter la priorite
			printf("\n\nErreur augmenter\n\n");
			exit(-1);
		}
		v->value = value;
		while (node->parent && vp->value > v->value) {  //on s'arrete quand il arrive a la racine ou a la bonne position
			CBTreeSwap(tree, node->parent, node);
			if (node->parent)
				vp = node->parent->data;
		}
	}
}

void viewCBTHeap(const Heap *H, void (*ptrf)(const void*)) {
	viewCBTree(H->heap, ptrf, 0);
}

/**********************************************************************************
 * ORDERED-LIST HEAP
 **********************************************************************************/

void* OLHeapInsert(Heap *H, int value, void* data) {
	
	HNode *node = newHNode(value, data);
    List *L = H->heap;
	LNode  *iterator = L->head;

	if (L->numelm == 0 )//la liste est vide
	{
		listInsertFirst(L, node);
		return L->head;
	}
	else//liste n'est pas vide
	{
		HNode *h = iterator->data;

		if (iterator->suc == NULL) {  // si la liste ne contient qu'un noeud
			if (h->value < node->value) {
				listInsertAfter(L, node, iterator);
				return iterator->suc;
			}
			else {
				listInsertFirst(L, node);
				return iterator->pred;
			}
		}
		else {
			LNode* noeud = iterator;
			while (iterator && h->value < node->value)//chercher la bonne position pour le node
			{
				iterator = iterator->suc;
				if (iterator)
					h = iterator->data;
				else
					break;
			}
			if (iterator) {
				if(iterator->pred)
					listInsertAfter(L, node, iterator->pred);
				else{
					listInsertAfter(L, node, iterator);
					listSwap(L, iterator, iterator->suc);
				}
				noeud = iterator->pred;
			}
			else {
				listInsertAfter(L, node, L->tail);
				noeud = L->tail;
			}
			return noeud;
		}
		
	}
}



HNode * OLHeapExtractMin(Heap *H) {
	LNode *E = listRemoveFirst(H->heap);
	return E->data;
}


void OLHeapIncreasePriority(Heap *H, void* lnode, int value) {
	
	List *L = H->heap;
	LNode *iterator = lnode;
	
	HNode *hnode = iterator->data;
	hnode->value = value;

	if (iterator->pred)
	{
		LNode *pred = iterator->pred;
		HNode* predData = pred->data;

		while (pred && hnode->value < predData->value)
		{
			listSwap(L, iterator->pred, iterator);
			pred = iterator->pred;
			if(pred)
				predData = pred->data;
			hnode = iterator->data;
		}
	}
}

void viewOLHeap(const Heap *H, void (*ptrf)(const void*)) {
	viewList(H->heap, ptrf);
}
	
