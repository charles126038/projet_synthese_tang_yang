#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include "list.h"
#include "tree.h"
#include "dyntable.h"
#include "heap.h"

static int compare_lists(List *l1, int l2[], int size) {
	if (l1->numelm != size)
		return 0;

	LNode *curr = l1->head;
	int i = 0;
	while (curr != NULL) {
		if ((*(int*)curr->data) != l2[i])
			return 0;
		curr = curr->suc;
		i++;
	}

	curr = l1->tail;
	i = size - 1;
	while (curr != NULL) {
		if ((*(int*)curr->data) != l2[i])
			return 0;
		curr = curr->pred;
		i--;
	}
	return 1;
}

void test_list() {
	int t = 0;
	int f = 0;

	fprintf(stderr, "Testing list functions ");

	int *i1 = malloc(sizeof(int));
	int *i2 = malloc(sizeof(int));
	int *i3 = malloc(sizeof(int));
	int *i4 = malloc(sizeof(int));
	*i1 = 1;
	*i2 = 2;
	*i3 = 3;
	*i4 = 4;

	int tab[4] = { *i1, *i2, *i3, *i4 };
	int t1[1] = { *i1 };
	int t2[2] = { *i2, *i1 };
	int t3[3] = { *i3, *i2, *i1 };
	int t4[4] = { *i4, *i3, *i2, *i1 };

	List *L = newList();
	listInsertLast(L, (int*)i1);
	if (compare_lists(L, tab, 1) == 0) { fprintf(stderr, "x"); f++; }
	else { fprintf(stderr, "."); t++; }
	listInsertLast(L, (int*)i2);
	if (compare_lists(L, tab, 2) == 0) { fprintf(stderr, "x"); f++; }
	else { fprintf(stderr, "."); t++; }
	listInsertLast(L, (int*)i3);
	if (compare_lists(L, tab, 3) == 0) { fprintf(stderr, "x"); f++; }
	else { fprintf(stderr, "."); t++; }
	listInsertLast(L, (int*)i4);
	if (compare_lists(L, tab, 4) == 0) { fprintf(stderr, "x"); f++; }
	else { fprintf(stderr, "."); t++; }


	deleteList(L, NULL);
	if (compare_lists(L, tab, 0) == 0) { fprintf(stderr, "x"); f++; }
	else { fprintf(stderr, "."); t++; }

	listInsertFirst(L, (int*)i1);
	if (compare_lists(L, t1, 1) == 0) { fprintf(stderr, "x"); f++; }
	else { fprintf(stderr, "."); t++; }
	listInsertFirst(L, (int*)i2);
	if (compare_lists(L, t2, 2) == 0) { fprintf(stderr, "x"); f++; }
	else { fprintf(stderr, "."); t++; }
	listInsertFirst(L, (int*)i3);
	if (compare_lists(L, t3, 3) == 0) { fprintf(stderr, "x"); f++; }
	else { fprintf(stderr, "."); t++; }
	listInsertFirst(L, (int*)i4);
	if (compare_lists(L, t4, 4) == 0) { fprintf(stderr, "x"); f++; }
	else { fprintf(stderr, "."); t++; }
}

static void afficher(void * n){
	printf("%d", *((int*)n));
}

void test_Remove(){

	fprintf(stderr, "Testing list functions..... \n");

	int *i1 = malloc(sizeof(int));
	int *i2 = malloc(sizeof(int));
	int *i3 = malloc(sizeof(int));
	int *i4 = malloc(sizeof(int));
	*i1 = 1;
	*i2 = 2;
	*i3 = 3;
	*i4 = 4;

	int t1[3] = { *i3, *i2, *i1 };
	int t2[3] = { *i4, *i3, *i2 };
	int t3[3] = { *i4, *i2, *i1 };
	int t4[4] = { *i3, *i1, *i2 };

	List *L = newList();

	listInsertFirst(L, (int*)i1);
	listInsertFirst(L, (int*)i2);
	listInsertFirst(L, (int*)i3);
	listInsertFirst(L, (int*)i4);

	//Tester la fonction RemoveLast
	LNode * last = listRemoveLast(L);
	if (compare_lists(L, t2, 3) != 0) {
		printf("\nFonction RemoveLast BON!!\nLe noed: %d\n", *((int*)last->data));
	}
	else {
		printf("\nFonction RemoveLast PAS BON!!\n");
	}

	//Tester la fonction RemoveFirst
	listInsertLast(L, (int*)i1);
	LNode * first = listRemoveFirst(L);
	if (compare_lists(L, t1, 3) != 0) {
		printf("\nFonction RemoveFirst BON!!\nLe noed: %d\n", *((int*)first->data));
	}
	else {
		printf("\nFonction RemoveFirst PAS BON!!\n");
	}

	//Tester la fonction RemoveNode
	listInsertFirst(L, (int*)i4);
	LNode * node = L->head->suc;
	LNode * nodesup = listRemoveNode(L, node);
	if (compare_lists(L, t3, 3) != 0) {
		printf("\nFonction RemoveNode BON!!\nLe noed: %d\n", *((int*)nodesup->data));
	}
	else {
		printf("\nFonction RemoveNode PAS BON!!\n");
	}

	//Tester la fonction Swap
	List *Lsw = newList();
	listInsertFirst(Lsw, (int*)i1);
	listInsertFirst(Lsw, (int*)i2);
	listInsertFirst(Lsw, (int*)i3);
	puts("\n");
	viewList(Lsw, &afficher);
	LNode *left = Lsw->head->suc;
	LNode *right = left->suc;
	listSwap(Lsw, left, right);
	if (compare_lists(Lsw, t4, 3) != 0) {
		printf("Fonction Swap BON!!\n");
	}
	else {
		printf("Fonction Swap PAS BON!!\n");
	}
	viewList(Lsw, &afficher);
	deleteList(L, NULL);
}

static void ConsArbre(TNode *parent, TNode *fg, TNode *fd){
	assert(parent != NULL);
	if (fd != NULL || fg != NULL) {
		if (fd == NULL) {
			parent->left = fg;
			fg->parent = parent;
		}
		else {
			parent->left = fg;
			fg->parent = parent;
			parent->right = fd;
			fd->parent = parent;
		}
	}
}

static int compare_arbre(TNode* v1, TNode* v2) {
	/** @Comparer les deux arbre
	* @v1 et v2 sont racines des arbre non null
	*/
	if (v1 != NULL || v2 != NULL) {
		if (v1->data != v2->data)
			return 0;
		else
			return 1;

		TNode* fg1 = v1->left;
		TNode* fg2 = v2->left;
		TNode* fd1 = v1->right;
		TNode* fd2 = v2->right;
		return compare_arbre(fg1, fg2) + compare_arbre(fd1, fd2);
	}
	else {
		return 1;
	}
}

void ParcourirArbre(CBTree* tree) {
	TNode* iter = tree->last;
	while (iter->parent)
		iter = iter->parent;
	if (iter == tree->root)
		printf("\nC'EST BON L'ARBRE\n");
}

void test_Arbre(){
	int *i1 = malloc(sizeof(int));
	int *i2 = malloc(sizeof(int));
	int *i3 = malloc(sizeof(int));
	int *i4 = malloc(sizeof(int));
	int *i5 = malloc(sizeof(int));
	int *i6 = malloc(sizeof(int));
	int* i7 = malloc(sizeof(int));
	int* i8 = malloc(sizeof(int));
	int* i9 = malloc(sizeof(int));
	int* i10 = malloc(sizeof(int));

	*i1 = 1;
	*i2 = 2;
	*i3 = 3;
	*i4 = 4;
	*i5 = 5;
	*i6 = 6;
	*i7 = 7;
	*i8 = 8;
	*i9 = 9;
	*i10 = 10;

	

	// constituer un arbre
	CBTree *tree = newCBTree();
	TNode *v0 = newTNode(i1);
	TNode *v1 = newTNode(i2);
	TNode *v2 = newTNode(i3);
	TNode *v3 = newTNode(i4);
	TNode *v4 = newTNode(i5);
	TNode *v5 = newTNode(i6);

	tree->numelm = 6;
	tree->root = v0;
	tree->last = v5;
	ConsArbre(v0, v1, v2);
	ConsArbre(v1, v3, v4);
	ConsArbre(v2, v5, NULL);

	printf("testing les fonctions Arbre...\n");
	
	// tester la fonction **Insert**
	printf("\ntesting fonction Insert...\n");
	CBTree* t = newCBTree();
	CBTreeInsert(t, i1);
	CBTreeInsert(t, i2);
	CBTreeInsert(t, i3);
	CBTreeInsert(t, i4);
	CBTreeInsert(t, i5);
	CBTreeInsert(t, i6);
	TNode* last = t->last;
	if (compare_arbre(t->root, tree->root) != 0 && last->data == v5->data)
		printf("Fonction Inssert BON!!!\n");
	else
		printf("Fonction Insert PAS BON!!\n");
	system("pause");
	

	//construire un arbre pour comparer
	CBTree* t0 = newCBTree();
	CBTreeInsert(t0, i1);
	CBTreeInsert(t0, i2);
	CBTreeInsert(t0, i3);
	CBTreeInsert(t0, i4);
	CBTreeInsert(t0, i5);
	CBTreeInsert(t0, i6);
	CBTreeInsert(t0, i7);
	CBTreeInsert(t0, i8);
	CBTreeInsert(t0, i9);
	CBTreeInsert(t0, i10);
	//  **** tester la fonction remove *****
	printf("\ntesting fonction Remove...\n");
	int *T1;
	T1 = CBTreeRemove(t0);
	int *T2;
	T2 = CBTreeRemove(t0);
	int *T3;
	T3 = CBTreeRemove(t0);
	CBTree* t7 = newCBTree();
	CBTreeInsert(t7, i1);
	CBTreeInsert(t7, i2);
	CBTreeInsert(t7, i3);
	CBTreeInsert(t7, i4);
	CBTreeInsert(t7, i5);
	CBTreeInsert(t7, i6);
	CBTreeInsert(t7, i7);

	if (compare_arbre(t0->root, t7->root) != 0 && t0->last->data == t7->last->data && T1 == i10 && T2 == i9 && T3 == i8)
		printf("Fonction Remove BON!!!\n");
	else
		printf("Fonction Remove PAS BON!!\n");
	system("pause");


	

	CBTree* t1 = newCBTree();
	TNode* c0 = newTNode(i1);
	TNode* c1 = newTNode(i2);
	TNode* c2 = newTNode(i3);
	TNode* c3 = newTNode(i4);
	TNode* c4 = newTNode(i5);
	TNode* c5 = newTNode(i6);
	t1->numelm = 6;
	t1->root = c0;
	t1->last = c5;
	ConsArbre(c0, c1, c2);
	ConsArbre(c1, c3, c4);
	ConsArbre(c2, c5, NULL);

	// **** tester la fonction Swap *****
	printf("\ntesting fonction Swap...\n");
	CBTreeSwap(t1, c2, c5);
	CBTreeSwap(t1, c0, c1);
	CBTreeSwap(t1, c1, c5);
	CBTree* t4 = newCBTree();
	TNode* a1 = newTNode(i1);
	TNode* a2 = newTNode(i2);
	TNode* a5 = newTNode(i3);
	TNode* a3 = newTNode(i4);
	TNode* a4 = newTNode(i5);
	TNode* a0 = newTNode(i6);
	t4->numelm = 6;
	t4->root = a0;
	t4->last = a5;
	ConsArbre(a0, a1, a2);
	ConsArbre(a1, a3, a4);
	ConsArbre(a2, a5, NULL);
	if (compare_arbre(t1->root, t4->root) == 0)
		printf("Fonction Swap PAS BON!!!\n");
	else
		printf("Fonction Swap BON!!\n");
	system("pause");

	//testing la fonction SwapRacineLast
	CBTree* t8 = newCBTree();
	CBTreeInsert(t8, i1);
	CBTreeInsert(t8, i2);
	CBTreeInsert(t8, i3);
	CBTreeInsert(t8, i4);
	CBTreeInsert(t8, i5);
	CBTreeInsert(t8, i6);

	printf("\ntesting fonction SwapRootLast...\n");
	CBTree* t9 = newCBTree();
	TNode* z5 = newTNode(i1);
	TNode* z1 = newTNode(i2);
	TNode* z2 = newTNode(i3);
	TNode* z3 = newTNode(i4);
	TNode* z4 = newTNode(i5);
	TNode* z0 = newTNode(i6);
	t9->numelm = 6;
	t9->root = z0;
	t9->last = z5;
	ConsArbre(z0, z1, z2);
	ConsArbre(z1, z3, z4);
	ConsArbre(z2, z5, NULL);
	CBTreeSwapRootLast(t8);
	if (compare_arbre(t8->root, t9->root) == 0)
		printf("Fonction SwapRootLast PAS BON!!!\n");
	else
		printf("Fonction SwapRootLast BON!!\n");
	system("pause");

}

static int compare_DTableau(DTable* dt, int d[]) {
	for (int i = 0; i < dt->used; i++) {
		if (*(int*)dt->T[i] != d[i])
			return 0;
	}
	return 1;
}

void test_dynTable() {
	int* i1 = malloc(sizeof(int));
	int* i2 = malloc(sizeof(int));
	int* i3 = malloc(sizeof(int));
	int* i4 = malloc(sizeof(int));
	int* i5 = malloc(sizeof(int));
	int* i6 = malloc(sizeof(int));

	*i1 = 1;
	*i2 = 2;
	*i3 = 3;
	*i4 = 4;
	*i5 = 5;
	*i6 = 6;

	DTable* dtab = newDTable();
	printf("testing dynTable...\n");

	//testing la fonction insert
	printf("\ntesting fonction Insert\n");
	DTableInsert(dtab, i1);
	DTableInsert(dtab, i2);
	DTableInsert(dtab, i3);
	DTableInsert(dtab, i4);
	DTableInsert(dtab, i5);
	DTableInsert(dtab, i6);
	int test1[6] = { *i1, *i2, *i3, *i4, *i5, *i6 };
	if (compare_DTableau(dtab, test1) != 0)
		printf("Fonction Insert BON!!!\n");
	else
		printf("Fonction Insert PAS BON!!\n");
	system("pause");


	//testing fonction swap
	printf("\ntesting fonction Swap\n");
	DTableSwap(dtab, 0, 2);
	DTableSwap(dtab, 4, 5);
	DTableSwap(dtab, 2, 3);
	int test2[6] = { *i3, *i2, *i4, *i1, *i6, *i5 };
	if (compare_DTableau(dtab, test2) != 0)
		printf("Fonction Swap BON!!!\n");
	else
		printf("Fonction Swap PAS BON!!\n");
	system("pause");


	//testing fonction Remove
	printf("\ntesting fonction Remove\n");
	int* v;
	v = DTableRemove(dtab);
	v = DTableRemove(dtab);
	v = DTableRemove(dtab);
	v = DTableRemove(dtab);
	v = DTableRemove(dtab);
	int test3[6] = { *i3 };
	if (compare_DTableau(dtab, test3) != 0 && v == i2)
		printf("Fonction Remove BON!!!\n");
	else
		printf("Fonction Remove PAS BON!!\n");
	system("pause");
}

static int compare_HeapArbre(TNode* v1, TNode* v2) {
	/** @ Comparer les deux arbre
	* @ v2 est racine de arbre non null
	* @ v1 est racine de HeapArbre non null
	*/
	if (v1 != NULL || v2 != NULL) {
		HNode* n1;
		if (v1 == NULL)
			return 1;

		n1 = v1->data;
		if (n1->data != v2->data)
			return 0;
		else
			return 1;

		TNode* fg1 = v1->left;
		TNode* fg2 = v2->left;
		TNode* fd1 = v1->right;
		TNode* fd2 = v2->right;
		return compare_arbre(fg1, fg2) + compare_arbre(fd1, fd2);
	}
	else {
		return 1;
	}
}

static int compare_HeapLists(List* l1, int l2[], int size) {
	if (l1->numelm != size)
		return 0;

	LNode* curr = l1->head;
	HNode* data = curr->data;
	int i = 0;
	while (curr != NULL) {
		if ((*(int*)data->data) != l2[i])
			return 0;
		curr = curr->suc;
		if(curr)
			data = curr->data;
		i++;
	}

	curr = l1->tail;
	data = curr->data;
	i = size - 1;
	while (curr != NULL) {
		if ((*(int*)data->data) != l2[i])
			return 0;
		curr = curr->pred;
		if(curr)
			data = curr->data;
		i--;
	}
	return 1;
}

static int compare_HeapTableau(DTable* dt, int d[]) {
	HNode* v;
	for (int i = 0; i < dt->used; i++) {
		v = dt->T[i];
		if (*(int*)v->data != d[i])
			return 0;
	}
	return 1;
}

void test_Heap() {
	int* i1 = malloc(sizeof(int));
	int* i2 = malloc(sizeof(int));
	int* i3 = malloc(sizeof(int));
	int* i4 = malloc(sizeof(int));
	int* i5 = malloc(sizeof(int));
	int* i6 = malloc(sizeof(int));

	*i1 = 1;
	*i2 = 2;
	*i3 = 3;
	*i4 = 4;
	*i5 = 5;
	*i6 = 6;

	printf("testing les fonctions HEAP...\n");

	/**********************************************************
	* testing **** Liste ****
	***********************************************************/
	printf("\n************testing les fonctions Liste...***************\n");
	Heap* Lheap = newHeap(2);

	//testing fonction insert
	printf("\ntesting fonction insert..\n");
	LNode* l1 = OLHeapInsert(Lheap, 1, i1);
	LNode* l2 = OLHeapInsert(Lheap, 3, i2);
	LNode* l3 = OLHeapInsert(Lheap, 4, i3);
	LNode* l4 = OLHeapInsert(Lheap, 2, i4);
	LNode* l5 = OLHeapInsert(Lheap, 5, i5);
	LNode* l6 = OLHeapInsert(Lheap, 6, i6);
	HNode* lh1 = l1->data;
	HNode* lh2 = l2->data;
	HNode* lh3 = l3->data;
	HNode* lh4 = l4->data;

	int tab[6] = { *i1, *i4, *i2, *i3, *i5, *i6 };
	if (compare_HeapLists(Lheap->heap, tab, 6) != 0 && lh1->data == i1 && lh2->data == i2 && lh3->data == i3 && lh4->data == i4)
		printf("Fonction Insert BON!!!\n");
	else
		printf("Fonction Insert PAS BON!!\n");
	system("pause");
	

	//testing fonction ExtractMin
	printf("\ntesting la fonction ExtractMin..\n");
	HNode *o1 = OLHeapExtractMin(Lheap);
	HNode* o2 = OLHeapExtractMin(Lheap);
	HNode* o3 = OLHeapExtractMin(Lheap);
	HNode* o4 = OLHeapExtractMin(Lheap);
	HNode* o5 = OLHeapExtractMin(Lheap);
	HNode* o6 = OLHeapExtractMin(Lheap);
	List* L = Lheap->heap;
	if(L->numelm == 0 && o6->data == i6 && L->head == NULL && L->tail == NULL && o5->data == i5 && o4->data == i3 && o3->data == i2 && o2->data == i4 && o1->data == i1)
		printf("Fonction ExtractMin BON!!!\n");
	else
		printf("Fonction ExtractMin PAS BON!!!\n");
	system("pause");
	

	//testing fonction IncreasePriority
	printf("\ntesting la fonction IncreasePriority...\n");
	l1 = OLHeapInsert(Lheap, 1, i1);
	l2 = OLHeapInsert(Lheap, 3, i2);
	l3 = OLHeapInsert(Lheap, 4, i3);
	l4 = OLHeapInsert(Lheap, 2, i4);
	l5 = OLHeapInsert(Lheap, 5, i5);
	l6 = OLHeapInsert(Lheap, 6, i6);

	OLHeapIncreasePriority(Lheap, l6, 0);
	OLHeapIncreasePriority(Lheap, l5, 2);

	int tab2[6] = { *i6,*i1,*i4,*i5,*i2,*i3 };
	if (compare_HeapLists(Lheap->heap, tab2, 6) != 0)
		printf("Fonction IncreasePriority BON!!!\n");
	else
		printf("Fonction IncreasePriority PAS BON!!\n");
	system("pause");
	

	/**********************************************************
	* testing **** ArbreBinaire ****
	***********************************************************/
	printf("\n***********testing les fonctions ArbreBinaire..*************\n");
	Heap* Theap = newHeap(1);


	//testing fonction insert
	printf("\ntesting fonction insert..\n");
	TNode* v1 = CBTHeapInsert(Theap, 6, i1);
	TNode* v2 = CBTHeapInsert(Theap, 5, i2);
	TNode* v3 = CBTHeapInsert(Theap, 4, i3);
	TNode* v4 = CBTHeapInsert(Theap, 3, i4);
	TNode* v5 = CBTHeapInsert(Theap, 2, i5);
	TNode* v6 = CBTHeapInsert(Theap, 1, i6);
	HNode* hb1 = v1->data;
	HNode* hb2 = v2->data;
	HNode* hb3 = v3->data;
	HNode* hb4 = v4->data;
	HNode* hb5 = v5->data;
	HNode* hb6 = v6->data;

	CBTree* test1 = newCBTree();
	CBTreeInsert(test1, i6);
	CBTreeInsert(test1, i4);
	CBTreeInsert(test1, i5);
	CBTreeInsert(test1, i1);
	CBTreeInsert(test1, i3);
	CBTreeInsert(test1, i2);
	CBTree* t = Theap->heap;
	if (compare_HeapArbre(t->root, test1->root) != 0 && hb1->data == i1 && hb2->data == i2 && hb3->data == i3 && hb4->data == i4 && hb5->data == i5 && hb6->data == i6)
		printf("Fonction Insert BON!!!\n");
	else
		printf("Fonction Insert PAS BON!!\n");
	system("pause");

	//testing fonction ExtractMin...
	printf("\ntesting fonction ExtractMin...\n");
	HNode* n1 = CBTHeapExtractMin(Theap);
	HNode* n2 = CBTHeapExtractMin(Theap);
	HNode* n3 = CBTHeapExtractMin(Theap);
	HNode* n4 = CBTHeapExtractMin(Theap);

	CBTree* test2 = newCBTree();
	CBTreeInsert(test2, i2);
	CBTreeInsert(test2, i1);
	if (compare_HeapArbre(t->root, test2->root) != 0 && n1->data == i6 && n2->data == i5 && n3->data == i4 && n4->data == i3)
		printf("Fonction ExtractMin BON!!!\n");
	else
		printf("Fonction ExtractMin PAS BON!!\n");
	system("pause");

	//testing fonction IncreasePriority
	printf("\ntesting la fonction IncreasePriority..\n");
	v3 = CBTHeapInsert(Theap, 4, i3);
	v4 = CBTHeapInsert(Theap, 3, i4);
	v5 = CBTHeapInsert(Theap, 2, i5);
	v6 = CBTHeapInsert(Theap, 1, i6);

	CBTHeapIncreasePriority(Theap, v1, 0);
	CBTHeapIncreasePriority(Theap, v4, 0);

	CBTree* test3 = newCBTree();
	CBTreeInsert(test3, i1);
	CBTreeInsert(test3, i4);
	CBTreeInsert(test3, i5);
	CBTreeInsert(test3, i6);
	CBTreeInsert(test3, i3);
	CBTreeInsert(test3, i2);
	if (compare_HeapArbre(t->root, test3->root) != 0)
		printf("Fonction IncreasePriority BON!!!\n");
	else
		printf("Fonction IncreasePriority PAS BON!!\n");
	system("pause");


	/**********************************************************
	* testing **** TableauDynamique ****
	***********************************************************/
	printf("\n***********testing les fonctions TableauDynamique..************\n");

	//testing fonction insert...
	printf("\ntesting la fonction Insert...\n");
	Heap* DTHeap = newHeap(0);
	int* d1 = DTHeapInsert(DTHeap, 5, i1);
	int* d2 = DTHeapInsert(DTHeap, 2, i2);
	int* d3 = DTHeapInsert(DTHeap, 3, i3);
	int* d4 = DTHeapInsert(DTHeap, 1, i4);
	int* d5 = DTHeapInsert(DTHeap, 6, i5);
	int* d6 = DTHeapInsert(DTHeap, 4, i6);
	int Dtest[6] = { *i4, *i2, *i3, *i1, *i5, *i6 };
	if (compare_HeapTableau(DTHeap->heap, Dtest) != 0 && *d1 == 0 && *d2 == 1 && *d3 == 2 && *d4 == 3 && *d5 == 4 && *d6 == 5)
		printf("Fonction Insert BON!!!\n");
	else
		printf("Fonction Insert PAS BON!!\n");
	system("pause");

	printf("\ntesting la fonction ExtractMin...\n");
	HNode *h1= DTHeapExtractMin(DTHeap);
	h1 = DTHeapExtractMin(DTHeap);
	h1 = DTHeapExtractMin(DTHeap);
	h1 = DTHeapExtractMin(DTHeap);
	h1 = DTHeapExtractMin(DTHeap);

	int Dtest1[1] = { *i5 };
	if (compare_HeapTableau(DTHeap->heap, Dtest1) != 0 && h1->data == i1)
		printf("Fonction ExtractMin BON!!!\n");
	else
		printf("Fonction ExtractMin PAS BON!!\n");
	system("pause");

	d2 = DTHeapInsert(DTHeap, 2, i2);
	d3 = DTHeapInsert(DTHeap, 3, i3);
	d4 = DTHeapInsert(DTHeap, 1, i4);
	d1 = DTHeapInsert(DTHeap, 5, i1);
	d6 = DTHeapInsert(DTHeap, 4, i6);

	//testing fonction IncreasePriority...
	printf("\ntesting la fonction IncreasePriority...\n");
	int ind = 4;
	int* pos = &ind;
	DTHeapIncreasePriority(DTHeap, pos, 0);

	int Dtest2[6] = { *i1, *i4, *i3, *i5, *i2, *i6 };
	if (compare_HeapTableau(DTHeap->heap, Dtest2) != 0)
		printf("Fonction IncreasePriority BON!!!\n");
	else
		printf("Fonction IncreasePriority PAS BON!!\n");
	system("pause");
}
