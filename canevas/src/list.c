#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "list.h"

List * newList() {
	List * L = calloc(1, sizeof(List));
	return L;
}

void deleteList(List * L, void (*ptrF)()) {
	LNode * iterator = L->head;

	if (ptrF == NULL) {
		while (iterator) {
			LNode * current = iterator;

			iterator = iterator->suc;
			free(current);
		}
	} else {
		while (iterator) {
			LNode * current = iterator;

			(*ptrF)(&current->data);
			free(current);
		}
	}
	L->head = NULL;
	L->tail = NULL;
	L->numelm = 0;
}

void viewList(const List * L, void (*ptrF)()) {
	printf("nb of nodes = %d\n", L->numelm);
	for(LNode * iterator = L->head; iterator; iterator = iterator->suc) {
		(*ptrF)(iterator->data);
		printf(" ");
	}
	printf("\n");
}

void listInsertFirst(List * L, void * data) {
	LNode * E = calloc(1, sizeof(LNode));

	E->data = data;
	E->suc = L->head;

	if(L->head == NULL)
		L->tail = E;
	else
		L->head->pred = E;
	L->head = E;
	L->numelm += 1;
}

void listInsertLast(List * L, void * data) {
	LNode * E = calloc(1, sizeof(LNode));

	E->data = data;
	E->pred = L->tail;

	if(L->tail == NULL)
		L->head = E;
	else
		L->tail->suc = E;
	L->tail = E;
	L->numelm += 1;
}

void listInsertAfter(List * L, void * data, LNode * ptrelm) {
	LNode * E = calloc(1, sizeof(LNode));

	E->data = data;
	E->pred = ptrelm;

	if(ptrelm == L->tail)
		L->tail = E;
	else
		ptrelm->suc->pred = E;
	E->suc = ptrelm->suc;
	ptrelm->suc = E;
	L->numelm += 1;
}

LNode* listRemoveFirst(List * L) {
	assert(L->head);
	LNode *iterator = L->head; //copie le premier noed
	if(L->numelm == 1)//si elle contient un seul noed
	{
		L->head = NULL;
		L->tail = NULL;
	}
	else
	{
    // découper le connection
	L->head = iterator -> suc; 
	L->head->pred = NULL;
	}

	L->numelm--;
	return iterator;
}

LNode* listRemoveLast(List * L) {
	assert(L->head);
	LNode * iterator = L->tail;  //copier le dernier noed
	if(L->numelm == 1){ //si elle contient un seul noed
		//decouper les connections
		L->head = NULL;
		L->tail = NULL;
	}else{
		//decouper les connections
		L->tail = L->tail->pred; 
		L->tail->suc = NULL;
	}
	L->numelm--;
	return iterator;
}

LNode* listRemoveNode(List * L, LNode * node) {
	if(L->numelm == 1){ //si elle contient un seul noed
		L->head = NULL;
		L->tail = NULL;
	}else{
		//decouper les connections
		node->pred->suc = node->suc;
		node->suc->pred = node->pred;
	}
	L->numelm--;

	return node;
}

void listSwap(List * L, LNode * left, LNode *right) {
	assert(left->suc == right && left == right->pred);
	
	if(L->numelm == 2){ //Si il existe seulement 2 noeds
		L->head = right;
		L->tail = left;
	}else if(L->head == left){ //Si left est  le premier noed
		right->suc->pred = left;
		left->suc = right->suc->pred;
		L->head = right;
	}else if(L->tail == right){ //Si right est le dernier noed
		left->pred->suc = right;
		right->pred = left->pred->suc;
		L->tail = left;
	}else{ //Si left et right sont dans la liste
		left->pred->suc = right;
   		right->suc->pred = left;
	}

	right->pred = left->pred;
	left->suc = right->suc;
	left->pred = right;
	right->suc = left;
}