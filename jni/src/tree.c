﻿#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "tree.h"

TNode * newTNode(void* data) {
	TNode *node = malloc(sizeof(TNode));
	node->data = data;
	node->left = NULL;
	node->parent = NULL;
	node->right = NULL;
	return node;
}

CBTree * newCBTree() {
	CBTree *tree = malloc(sizeof(CBTree));
	tree->last = NULL;
	tree->numelm = 0;
	tree->root = NULL;
	return tree;
}

static void preorder(TNode *node, void (*ptrF)(const void*)) {
	if (node != NULL) {
		ptrF(node->data);
		printf(" ");
		preorder(node->left, ptrF);
		preorder(node->right, ptrF);
	}
}

static void inorder(TNode *node, void (*ptrF)(const void*)) {
	if (node != NULL) {
		inorder(node->left, ptrF);
		ptrF(node->data);
		printf(" ");
		inorder(node->right, ptrF);
	}
}

static void postorder(TNode *node, void (*ptrF)(const void*)) {
	if (node != NULL) {
		postorder(node->left, ptrF);
		postorder(node->right, ptrF);
		ptrF(node->data);
		printf(" ");
	}
}

// order = 0 (preorder), 1 (postorder), 2 (inorder)
void viewCBTree(const CBTree* tree, void (*ptrF)(const void*), int order) {
	assert(order == 0 || order == 1 || order == 2);
	printf("nb of nodes = %d\n", tree->numelm);
	switch (order) {
		case 0:
			preorder(tree->root, ptrF);
			break;
		case 1:
			postorder(tree->root, ptrF);
			break;
		case 2:
			inorder(tree->root, ptrF);
		break;
	}
	printf("\n");
}

void CBTreeInsert(CBTree* tree, void* data) {
	TNode *noeud = newTNode(data);
	if (tree->numelm == 0) {  //si c'est un arbre vide
		tree->root = noeud;
	}
	else if (tree->numelm == 1) {  //si il a un noeud
		tree->root->left = noeud;
		noeud->parent = tree->root;
	}
	else if (tree->numelm == 2) {  //si il a deux noeuds
		tree->root->right = noeud;
		noeud->parent = tree->root;
	}
	else {

		if (tree->last == tree->last->parent->left) {  //si last est le fils gauch de son pere
			tree->last->parent->right = noeud;
			noeud->parent = tree->last->parent;
		}
		else {  //si il est le fils droit
			TNode* iterator = tree->last;
			while (iterator->parent != NULL) {
				if (iterator == iterator->parent->right)  //on monte si le noeud courant est le fils droit de son pere
					iterator = iterator->parent;
				else
					break;  //on sort la boucle si le noeud courant est le fils gauche de son pere
			}

			if (iterator == tree->root)  //si le noeud courant est la racine
			{
				while (iterator->left != NULL) { //on parcourt directement vers la gauche
					iterator = iterator->left;
				}
				iterator->left = noeud;
				noeud->parent = iterator;
			}
			else { //si le noeud curant n'est pas la racine
				iterator = iterator->parent->right; //on passe a son frere
				while (iterator->left != NULL) { //on parcourt vers la gauche
					iterator = iterator->left;
				}
				iterator->left = noeud;
				noeud->parent = iterator;
			}
		}
	}
	tree->last = noeud;
	tree->numelm++;
}


void * CBTreeRemove(CBTree* tree) {
	assert(tree->last != NULL);
	void *v = tree->last->data;

	TNode *iterator;
	iterator = tree->last;
	if (tree->numelm == 1) {
		tree->last = NULL;
		tree->root = NULL;
	}
	 else if (tree->numelm == 2)//il y a deux noeuds
	{
		tree->last = tree->root;
		if(iterator == iterator->parent->left)
			iterator->parent->left = NULL;
		if (iterator == iterator->parent->right)
			iterator->parent->right = NULL;
	}
	else if (iterator == iterator->parent->left)  //last est fils gauche
	{

		TNode *A = tree->last;
		while (A == A->parent->left && A->parent->parent != NULL)
		{
			A = A->parent;
		}


		if (A->parent->parent== NULL)//last est le premier noeud dans sa nivau
		{
			if (A == A->parent->right)
			{
				A = A->parent->left;
			}
			else
			{
				A = A->parent->right;
			}
				
			
			
		}
		else//last n'est pas le premier noeud dans sa nivau
		{
			A = A->parent->left;
		}


		while (A && A->left != NULL)
		{
			A = A->right;
		}

		iterator->parent->left = NULL;
		
		tree->last = A;
	}
	else {  //last est fils droite
		tree->last = tree->last->parent->left;
		iterator->parent->right = NULL;
	}
	iterator->parent = NULL;
	tree->numelm--;
	return v;
}
void CBTreeSwap(CBTree* tree, TNode* parent, TNode* child) {
	assert(parent != NULL && child != NULL && (child == parent->left || child == parent->right));
	assert(parent != NULL && child != NULL && (child == parent->left || child == parent->right));
	TNode* fg = child->left;
	TNode* fd = child->right;
	TNode* par = parent->parent;

	if (child == parent->left) {  //si le child est le fils gauche du parent
		child->left = parent;
		child->right = parent->right;
		if (parent->right != NULL)
			child->right->parent = child;
	}
	else {  //sinon
		child->right = parent;
		child->left = parent->left;
		if (parent->left != NULL)
			child->left->parent = child;
	}

	if (parent == tree->root) {  //si le parent est la racine
		tree->root = child;
		child->parent = NULL;
	}
	else {
		child->parent = par;
		if (parent == par->left)  //si le parent est le fils gauche de son parent
			par->left = child;
		else
			par->right = child;
	}

	if (tree->last == child)
		tree->last = parent;
	if (tree->root == parent)
		tree->root = child;

	//changer le parent des fils de child
	if (fg != NULL)
		fg->parent = parent;
	if (fd != NULL)
		fd->parent = parent;
	//changer les fils et parent du parent
	parent->left = fg;
	parent->right = fd;
	parent->parent = child;
	
}


void CBTreeSwapRootLast(CBTree* tree) {
	if (tree->numelm > 1) {  //il faut avoir au moins deux noeuds
		TNode* parentL = tree->last->parent;   // le parent du fils last
		TNode* last = tree->last;
		if (tree->numelm == 2) {  // si il existe que deux noeuds
			if (tree->root->left == NULL) {
				last->parent = NULL;
				last->right = parentL;
				parentL->right = NULL;
				parentL->parent = last;
			}
			else {
				last->parent = NULL;
				last->left = parentL;
				parentL->left = NULL;
				parentL->parent = last;
			}
		}
		else if (tree->numelm == 3) {  // si il existe 3 noeuds
			tree->root->right = NULL;
			last->parent = NULL;
			last->right = tree->root;
			tree->root->parent = last;
			last->left = tree->root->left;
			tree->root->left->parent = last;
			tree->root->left = NULL;
		}
		else {
			
			TNode* iterator = last;
			while (iterator->parent->parent) {  // on cherche le noeud last est dans fils gauche ou fils droite de la racine
				iterator = iterator->parent;
			}
			tree->root->parent = parentL;
			last->parent = NULL;
			if (iterator == tree->root->left) {   //c'est dans le fils gauche
				tree->root->left = NULL;
				last->right = tree->root->right;
				tree->root->right->parent = last;
				tree->root->right = NULL;
				last->left = iterator;
				iterator->parent = last;
				if (last == parentL->left)  // si last est le fils gauche de son parent
					parentL->left = tree->root;
				else
					parentL->right = tree->root;
			}
			else {   //c'est dans le fils droite
				tree->root->right = NULL;
				last->left = tree->root->left;
				tree->root->left->parent = last;
				tree->root->left = NULL;
				last->right = iterator;
				iterator->parent = last;
				if (last == parentL->left)  // si last est le fils gauche de son parent
					parentL->left = tree->root;
				else
					parentL->right = tree->root;
			}
		}
		tree->last = tree->root;
		tree->root = last;
	}
}
