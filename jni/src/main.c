#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "dyntable.h"
#include "tree.h"
#include "list.h"
#include "heap.h"
#include "town.h"
#include "road.h"
#include "graph.h"
#include "test.h"

/**
 * Exemple d'une fonction qui affiche le contenu d'un HNode.
 * A modifier si besoin.
 */
void viewHNode(const HNode *node) {
	struct town * town = node->data;
	printf("(%d, %s)", node->value, getTownName(town));
}

/**
 * Affichage de la solution de l'algorithme de Dijkstra.
 * Pour chaque ville du graphe G on doit déjà avoir défini
 * les valeurs dist et pred en exécutant l'algorithme de Dijkstra.
 */
void viewSolution(graph G, char * sourceName) {
	printf("Distances from %s\n", sourceName);
	LNode * iterator;
	for (iterator = G->head; iterator; iterator = iterator->suc) {
		if (strcmp(getTownName(iterator->data), sourceName) != 0)
			printf("%s : %d km (via %s)\n", getTownName((struct town *) iterator->data),
											((struct town *) iterator->data)->dist,
											getTownName(((struct town *) iterator->data)->pred));
	}
}



/**
 * Algorithme de Dijkstra
 * inFileName : nom du fichier d'entrée
 * outFileName : nom du fichier de sortie
 * sourceName : nom de la ville de départ
 * heaptype : type du tas {0--tableaux dynamiques, 1--arbres binaires complets, 2--listes ordonnées}
 */
void Dijkstra(char * inFileName, char * outFileName, char * sourceName, int heaptype) {
	graph G = readmap(inFileName);
	FILE* fp = NULL;
	viewmap(G);
	fp = fopen(outFileName, "w");


	switch (heaptype) {
	/***************************************************************
	* *******            Tableux Dynamique             ************
	****************************************************************/
	case 0: {
		Heap* heap = newHeap(0);
		LNode* iterator = G->head;
		struct town* t = iterator->data;
		//initialiser
		while (iterator) {
			if (strcmp(getTownName(t), sourceName) == 0) {  //on mettre la priorite de la sourceVille a 0
				t->ptr = heap->HeapInsert(heap, 0, t);
				t->dist = 0;
			}
			else {// on initialise  les autres villes dans heap
				t->ptr = heap->HeapInsert(heap, INFTY, t);
				t->dist = INFTY;
			}
			t->pred = NULL;
			iterator = iterator->suc;
			if(iterator)
				t = iterator->data;
		}
	
		HNode* u;
		while (u = heap->HeapExtractMin(heap)) {  // on sort le sommet fixe
			t = u->data;
			LNode* iter = t->alist->head;
			struct road* r = iter->data;
			while (iter) {  // on parcourt tous les voisins du sommet fixe
				struct town* v;
				if (r->U == t)
					v = getVRoad(r);
				else
					v = getURoad(r);
				
				if (v->dist > (t->dist + r->km)) {
					v->dist = t->dist + r->km;
					v->pred = t;
					int ind = *(int*)v->ptr;
					heap->HeapIncreasePriority(heap, heap->dict->T[ind], v->dist);
				}

				iter = iter->suc;
				if(iter)
					r = iter->data;
			}
		}
	}break;

	/***************************************************************
	* *******            Arbres Binaires             ************
	****************************************************************/
	case 1:{
		Heap * heap = newHeap(1);
		LNode* iterator = G->head;
		struct town* t = iterator->data;
		//initialiser
		while (iterator) {
			if (strcmp(getTownName(t), sourceName) == 0) {  //on mettre la priorite de la sourceVille a 0
				t->ptr = heap->HeapInsert(heap, 0, t);
				t->dist = 0;
			}
			else {// on initialise  les autres villes dans heap
				t->ptr = heap->HeapInsert(heap, INFTY, t);
				t->dist = INFTY;
			}
			t->pred = NULL;
			iterator = iterator->suc;
			if (iterator)
				t = iterator->data;
		}
		HNode* u;
		while (u = heap->HeapExtractMin(heap)) {  // on sort le sommet fixe
			t = u->data;
			LNode* iter = t->alist->head;
			struct road* r = iter->data;
			while (iter) {  // on parcourt tous les voisins du sommet fixe
				struct town* v;
				if (r->U == t)
					v = getVRoad(r);
				else
					v = getURoad(r);

				if (v->dist > (t->dist + r->km)) {
					v->dist = t->dist + r->km;
					v->pred = t;
					heap->HeapIncreasePriority(heap, v->ptr, v->dist);
				}

				iter = iter->suc;
				if (iter)
					r = iter->data;
			}
			CBTree* tree = heap->heap;
			if (tree->numelm == 0)
				break;
		}
	}break;

	/***************************************************************
	* *******            Listes Ordonnées             ************
	****************************************************************/
	case 2:{
		Heap * heap = newHeap(2);
		LNode* iterator = G->head;
		struct town* t = iterator->data;
		//initialiser
		while (iterator) {
			if (strcmp(getTownName(t), sourceName) == 0) {  //on mettre la priorite de la sourceVille a 0
				t->ptr = heap->HeapInsert(heap, 0, t);
				t->dist = 0;
			}
			else {// on initialise  les autres villes dans heap
				t->ptr = heap->HeapInsert(heap, INFTY, t);
				t->dist = INFTY;
			}
			t->pred = NULL;
			iterator = iterator->suc;
			if (iterator)
				t = iterator->data;
		}
		HNode* u;
		while (u = heap->HeapExtractMin(heap)) {  // on sort le sommet fixe
			t = u->data;
			LNode* iter = t->alist->head;
			struct road* r = iter->data;
			while (iter) {  // on parcourt tous les voisins du sommet fixe
				struct town* v;
				if (r->U == t)
					v = getVRoad(r);
				else
					v = getURoad(r);

				if (v->dist > (t->dist + r->km)) {
					v->dist = t->dist + r->km;
					v->pred = t;
					heap->HeapIncreasePriority(heap, v->ptr, v->dist);
				}

				iter = iter->suc;
				if (iter)
					r = iter->data;
			}
			List* L = heap->heap;
			if (L->numelm == 0)
				break;
		}
	}break;

	default: { printf("Erreur heaptype\n"); exit(-1); }
	}
	
	// Les mettre dans le fichier
	LNode* iterator = G->head;
	fprintf(fp, "%d\n", G->numelm);
	fprintf(fp, "%s\n", sourceName);
	while (iterator) {
		struct town* t = iterator->data;
		if (strcmp(getTownName(t), sourceName) != 0) {  //C'est pas la ville departe
			struct town* tpre = t->pred;
			fprintf(fp, "%s %d %s\n", getTownName(t), t->dist, getTownName(tpre));
		}
		iterator = iterator->suc;
	}
	fclose(fp);
}

int main() {
	Dijkstra("E:/Informatique/L2/projet_synthese/projet_synthese_tang_yang/canevas/data/map2", "E:/Informatique/L2/projet_synthese/projet_synthese_tang_yang/canevas/data/out", "Metz", 1);
	//test_list();
	//test_Remove();
	//test_Arbre();
	//test_dynTable();
	//test_Heap();
	return EXIT_SUCCESS;
}
