#include "Traitement_traiter.h"
#include "dyntable.h"
#include "graph.h"
#include "heap.h"
#include "list.h"
#include "road.h"
#include "town.h"
#include "tree.h"

JNIEXPORT void JNICALL Java_Traitement_traiter_Dijkstra
  (JNIEnv * env, jobject obj, jstring f1, jstring f2, jstring v, jint n){
  	const char *inFile = (*env)->GetStringUTFChars(env, f1, NULL);
  	const char *outFile = (*env)->GetStringUTFChars(env, f2, NULL);
  	const char *villeDep = (*env)->GetStringUTFChars(env, v, NULL);
  	const int heaptype = n;
  	Dijkstra(inFile, outFile, villeDep, heaptype);
  } 
