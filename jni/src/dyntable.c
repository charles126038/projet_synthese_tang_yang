#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "dyntable.h"

DTable * newDTable() {
	DTable* DT = malloc(sizeof(DTable));
	DT->size = 1;
	DT->used = 0;
	DT->T = (void**)calloc(DT->size, sizeof(void*));
	DT->T[0] = NULL;
	return DT;
}

/**
 * @brief
 * Dédoubler la taille du tableau dtab
 */
static void scaleUp(DTable *dtab) {
	dtab->size = dtab->size * 2;
	void** newTab = (void**)realloc(dtab->T, dtab->size * sizeof(void*));
	assert(newTab != NULL);
	dtab->T = newTab;
}

/**
 * @brief
 * Diviser par 2 la taille du tableau dtab
 */
static void scaleDown(DTable *dtab) {
	dtab->size = dtab->size / 2;
	void** newTab = (void**)realloc(dtab->T, dtab->size * sizeof(void*));
	assert(newTab != NULL);
	dtab->T = newTab;
}

void viewDTable(const DTable *dtab, void (*ptrf)(const void*)) {
	int i;
	printf("size = %d\n", dtab->size);
	printf("used = %d\n", dtab->used);
	for (i = 0; i < dtab->used; i++) {
		ptrf(dtab->T[i]);
		printf(" ");
	}
	printf("\n");
}

void DTableInsert(DTable *dtab, void *data) {
	if (dtab->used < dtab->size) {
		dtab->T[dtab->used] = data;
		dtab->used++;
	}
	else {
		scaleUp(dtab);
		dtab->T[dtab->used] = data;
		dtab->used++;
	}
}

void * DTableRemove(DTable *dtab) {
	assert(dtab->used > 0);
	if (dtab->size / 4 < dtab->used) {
		dtab->used--;
		return dtab->T[dtab->used];
	}
	else {
		void* e = dtab->T[dtab->used - 1];
		dtab->T[dtab->used - 1] = NULL;
		scaleDown(dtab);
		dtab->used--;
		return e;
	}
}

void DTableSwap(DTable *dtab, int pos1, int pos2) {
	assert(pos1 < dtab->used);
	assert(pos2 < dtab->used);
	void* aux = dtab->T[pos1];
	dtab->T[pos1] = dtab->T[pos2];
	dtab->T[pos2] = aux;
}
